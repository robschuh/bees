<?php

namespace App\Controllers;
use App\Controllers\BaseController as Base;

class BeesController extends Base {
     
    // Get all Bees
    function index(){
         $bees = Bees::all();
         
         $view = explode('Controller',  get_class($this));
         
         return $this->view();
    }
}