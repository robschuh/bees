<?php

namespace App\Models;
use App\Models\Bees;

class Worker extends Bees {

    static $fast = 70;
    
     // Power losted each time the bee flys.
    static $power = 20;
    // When we create a new object (instance) of this type (Worker), we set its life by default, 
    // calling the parent class setLife, because $life is a protected property and we cannt call it directly.
    public function __construct($attributes = array()) {
        $this->setLife(20);
        $attributes['type'] = 'Worker';  
        parent::__construct($attributes);
    }
}
