<?php

namespace App\Models;
use App\Models\Bees as BeeBase;

class Drone extends BeeBase {

    static $fast = 40;
    
    // Power losted each time the bee flys.
    static $power = 30;
    
    public function __construct($attributes = array()) {
        $this->setLife(10);
        $attributes['type'] = 'Drone';  
        parent::__construct($attributes);
    }
}
