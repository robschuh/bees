<?php

namespace App\Models;

use ActiveRecord\Model as ActiveRecordModel;

class Beedb extends ActiveRecordModel {
    public function __construct($attributes){
        Beedb::initConnect();         
        parent::__construct($attributes, $this->guard_attributes, $instantiating_via_find=false, $this->new_record);

    }
    static function initConnect() {
        \ActiveRecord\Config::initialize(function($cfg) {
           $cfg->set_model_directory('src/models');
            $cfg->set_connections(array(
                'development' => 'mysql://root:usbw@localhost:3307/bees?charset=utf8'));
        }); 
    }

}
