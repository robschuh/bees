<?php


/*
 * Example to understand inherit, overriding and Interfaces.
 */

namespace App\Models;
use App\Models\Beedb;

use ActiveRecord\Model as ActiveRecordModel;

// NOTES:
// THE MOST IMPORTANT THINGS ABOUT AN INTERFACE.
// A Interface it is like a contract we have to follow when a class is implementing this contract.
// A interface NEVER could be instantiated, you only can instantiate the classes whose implement this interface.
Interface BeesInterface {
    
    /*
     |--------------------------------------------------------------------------
     | Bees model
     |--------------------------------------------------------------------------
     |
     | These are the basic methods MANDATORIES for all bees.
     |
     */
    
    public function setType();
  
    /**
     * Bee name.
     *
     * @var string
     */
    public function setName($name);
     
    public function getName();
    
    /**
     * Bee new life value.
     *
     * @var integer
     */
    public function setLife($life);

    public function getLife();
    
    public function getMiles();


}

// When you create this abstract class, you should thing about what a bee does? what it is commong for all the bees types?
class Bees extends Beedb implements BeesInterface {

    static $table_name = 'bees';
    protected $type;
    static $primary_key = 'id';
    public $name;
    protected $life;
    protected $miles;
    protected $new_record = false;
    protected $guard_attributes = true;
    
    static $fast = 1;
    static $power = 1;
    static $attr_accessible = array('id', 'name', 'life', 'miles');
    
    protected function fly($mts){}

    public function __construct($attributes = array()) {
  
        // Fill all the object attributes.
        if (!empty($attributes)) {
          //  var_dump($attributes); die;
            foreach ($attributes as $attrName => $attrValue) {
                if($attrName != 'id' && $attrName != 'type' && $attrName != 'miles'){
                    $dynamicMethodSet = 'set' .ucfirst($attrName);
                    $dynamicMethodGet = 'get' .ucfirst($attrName);
                   // var_dump($method); die;
                    $this->$dynamicMethodSet($attrValue);
                    $attributes[$attrName] = $this->$dynamicMethodGet();
                    $this->new_record = true;
                    $this->guard_attributes = false;
                }
            }
        }
        if(!isset($attributes['id'])){
            $this->new_record = true; //var_dump( $this->new_record ); die;
        }else{
            $this->new_record = false;
        }
        
        $attributes['life'] = $this->getLife();
        parent::__construct($attributes, $this->guard_attributes, $instantiating_via_find=false, $this->new_record);
 
    }
     
    public function setType() {
        $this->type = get_class($this);
    }

    public function setName($name){
         $this->name = $name;
    } 
    
    public function getName(){
        return $this->name;
    }   
    

    // Bee resurrection.
    public function setLife($life) {
        $this->life = $life;
    }

    // Get bee's life.
    public function getLife() {
        return $this->life;
    }

    // get bee's miles.
    public function getMiles() {
        return $this->miles;
    }


}
