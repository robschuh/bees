<?php

namespace App\Models;
use App\Models\Bees;

class Queen extends Bees {

    static $fast = 100;
    
    // Power losted each time the bee flys.
    static $power = 5;
    // When we create a new object (instance) of this type (Queen), we set its life by default, 
    // calling the parent class setLife, because $life is a protected property and we cannt call it directly.
    public function __construct($attributes = array()) {
        $this->setLife(50);
        $attributes['type'] = 'Queen';
        parent::__construct($attributes);
    }
}
