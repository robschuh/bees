<div class="col-8 col-md-auto">
    <h2>Bees availables</h2>
    <?php $bees = Bees::all(); ?>
     <table class="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Life</th>
            <th>Miles</th>
            <th>Fast</th>
            <th>Play</th>
            <th>#Id</th>
          </tr>
        </thead>
        <tbody>   
            <?php foreach($bees as $bee) : $beeType = $bee->type;?>
                <tr>
                    <td><a href="/bee/<?php $bee->name ?>"><?php echo $bee->name ?></a></td>
                    <td><?php echo $bee->type; ?></td>
                    <td><?php echo $bee->life; ?></td>
                    <td><?php echo $bee->miles; ?></td>
                    <td><?php echo $beeType::$fast ; ?></td>
                    <td><a href="/bees/play/<?php echo $bee->id; ?>"><span class="glyphicon glyphicon glyphicon-play-circle" aria-hidden="true">Play</span></a></td>
                    <td><?php echo $bee->id; ?></td>
                </tr>                              
            <?php endforeach;?>
        </tbody>  
     </table>
    </div>