<html Doctype!>
      <head>
        <meta charset="utf-8>
              <meta name=viewpoint" content="width=device-width">
        <meta name="keyword" content="martin, property, social housing"> 
        <meta name="author" content="martin gurps lawerance">
        <title>Bees game</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

        <link rel="stylesheet" type="text/css" href="css/styles.css">
    </head>
    <body>
        <?php
 
        /*
         * Main controller to access to the bees game
         */
        require __DIR__ . '/vendor/autoload.php';

        use App\Controllers\BeesController as ctl; 
        use App\Models\Bees as Bees;
        use App\Models\Bees\Queen;
        use App\Models\Bees\Worker;
        use App\Models\Bees\Drone;
        use App\Models\Beedb as db;
        db::initConnect(); 

        // Add new bee, and start palying the game.
        if (isset($_POST['type'])) {  
            
            $beeName = trim(strtolower($_POST['name'])); // Normaly we would like to make some validations.
            $beeType = trim(ucfirst(strtolower($_POST['type']))); // Get the bee type in the correct format.  
             
            // Using activeRecord library. Is this bee already in our db?
            $bee = Bees::find('first', array('conditions' => array('name = ?', $beeName)));
            
            // Check if bee already exists in our db.
            if (!isset($bee)) {
                //var_dump($beeType); die;
                // Add a new bee to our db.
                $bee = new $beeType(array('name' => $beeName));              
                $bee->save();
                if(!$bee){
                    echo 'Bee has not been created, please try again later <a href="/bees">Play again</a>';
                }

            }
    
            unset($bee);
        } 
        
        $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $query = '';  
        $parts = parse_url($url);//
        $paramms = explode('/', $parts['path']);     
        $action = isset($paramms[2]) ?  $paramms[2] : ''; // Param 2 is the action
        
        if(!empty($action) && $action != 'index.php') {
            $controller = $paramms[1]; // Param 1 is the controller
            $id = trim($paramms[3]); // Param 3 is the id
            $bee = Bees::find_by_id($id);

            if($bee){

                $beeType = $bee->type;
                $beeType::$fast = $beeType::$fast - round($bee->miles / 100);
                $bee->life = $bee->life - $beeType::$power; 

                $bee->miles = $bee->miles + $beeType::$fast;
                if($bee->life < 1){
                    $bee->delete();
                    unset($bee);
                }else{
                    $bee->save(); 
                }
            }
            if(!isset($bee)){
                echo 'this Bee is dead <a href="/bees">Play again</a>';
                exit;
            }
            ?>
            <table class="table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Life</th>
                    <th>Miles</th>
                    <th>Play</th>
                    <th>#Id</th>
                    <th>Foto</th>
                  </tr>
                </thead>
                <tbody>   
                    <tr>
                        <td><a href="/bee/<?php $bee->name ?>"><?php echo $bee->name ?></a></td>
                        <td><?php echo $bee->type; ?></td>
                        <td><?php echo $bee->life; ?></td>
                        <td><?php echo $bee->getMiles(); ?></td>
                        <td><a href="/bees/play/<?php echo $bee->id; ?>"><span class="glyphicon glyphicon glyphicon-play-circle" aria-hidden="true">Play</span></a></td>
                        <td><?php echo $bee->id; ?></td>
                        <td><img src="https://placebear.com/50/50" /></td>
                    </tr>                              
                </tbody>  
            </table>
        <?php
        }
        //$bee->letsPlay(5);
        ?>

        <div class="container">
            <div class="row">
                <div class="col-8 col-md-auto">
                    <h4>Create a Bee</h4>
                    <form action="index.php" name="flyBee" method="POST">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Bee Name</label>
                                <input type="text" class="form-control" id="beeName" placeholder="Bee name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="exampleSelect1">Bee type</label>
                                <select class="form-control" id="exampleSelect1" name="type">
                                    <option value="worker">Worker</option>
 				    <option value="drone">Drone</option>
                                    <option value="queen">Queen</option> 
                                </select>
                                <button type="submit" class="btn btn-primary" name="newBee">Add Bee</button>

                            </div>

                        </div>
                    </form>
                </div>  
                <div class="col-8 col-md-auto">
                    <h2>Bees availables</h2>
                    <?php $bees = App\Models\Bees::all(); ?>
                     <table class="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Life</th>
                            <th>Miles</th>
                            <th>Fast</th>
                            <th>Play</th>
                            <th>#Id</th>
                          </tr>
                        </thead>
                        <tbody>   
                            <?php foreach($bees as $bee) : $beeType = $bee->type;?>
                                <tr>
                                    <td><a href="/bee/<?php $bee->name ?>"><?php echo $bee->name ?></a></td>
                                    <td><?php echo $bee->type; ?></td>
                                    <td><?php echo $bee->life; ?></td>
                                    <td><?php echo $bee->miles; ?></td>
                                    <td><?php echo $beeType::$fast ; ?></td>
                                    <td><a href="/bees/play/<?php echo $bee->id; ?>"><span class="glyphicon glyphicon glyphicon-play-circle" aria-hidden="true">Play</span></a></td>
                                    <td><?php echo $bee->id; ?></td>
                                </tr>                              
                            <?php endforeach;?>
                        </tbody>  
                     </table>
                </div>
            </div>
        </div>


    </body>
</html>
