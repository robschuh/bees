# README #

REQUIREMENTS
Php 5.3 or up
Composer is needed.

# ActiveRecord Documentation
http://www.phpactiverecord.org/projects/main/wiki/Quick_Start

### What is this repository for? ###

Learning proposal
* Introduce to psr-4
* Learn namespaces
* Lean autoloading with compsoer

### How do I get set up? ###

* Create bees database and bees table
* Import sql from SQL folder
* update composer -> php composer update
* rebuild classes -> php composer dump-autoload 
* -> php composer dump -o
